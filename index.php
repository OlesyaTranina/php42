<?php
include 'Textsql.php';

spl_autoload_register(function () {
	$filePath = str_replace('/', DIRECTORY_SEPARATOR,$_SERVER['DOCUMENT_ROOT'] . 'Textsql.php');
	if (file_exists($filePath)) {
		include $filePath;
	}
});

use source\Todo;

$tasks = new Todo( "tranina","tranina", "neto0391");


if (isset($_POST["submitAdd"])) {
	$tasks->addRecord($_POST["description"],(isset($_POST["is_done"]))?1:0);
} else {if (isset($_POST["submitChange"])){
	$tasks->changeRecord($_POST["id"],$_POST["description"],(isset($_POST["is_done"]))?1:0);
} else {if (isset($_POST["submitDel"])){
	$tasks->delRecord($_POST["id"]);
};
};
};
$arrHeader = source\getHeader();
$strOrder = "";
foreach ($arrHeader as $key => $value) {
	if (isset($_POST["or_".$key])) {
		$strOrder = $key;
		break;
	};
}
$sqlRes = $tasks->getFullTable($strOrder);
$sqlRes->execute();
$editField = source\getEditField();
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>4.2 Задание к занятию «Запросы SELECT, INSERT, UPDATE и DELETE»</title>
	<style type="text/css">
		form {
			margin: 10px 0;	
		}
		.row {
			margin: 0 0;	
		}
		
		div {
			display: table;
		}
		ul {
			display: table-row;
		}
		li {
			display: table-cell;
			padding: 0 5px;
			vertical-align: middle;
			border: 1px solid;
			width:200px;
		}
		.header {
			font-weight: 700;
			text-align: center;
			background-color: lightblue;
		}
		
	</style>
</head>
<body>
	
	<h1>Список дел</h1>
	<div>
		<form class="header" action="index.php" method="post">	
			<ul>
				<?php
				foreach ($arrHeader as $key => $header) {?>
				<li class="header">
					<?= $header ?>
					<input type="submit" name=<?php  echo '"or_'. $key .'"'?> value="сортировать"></li>
					<?php	} ?>
					<li class="header">изменить</li>
					<li class="header">удалить</li>
				</ul>
			</form>
			<?php
			foreach ($sqlRes as $item) {	
				?>

				<form class="row" action="index.php" method="post">	
					<ul>
						<?php	foreach ($arrHeader as $key => $header) {
							?>
							<li > <input type=<?php switch ($key){case "is_done":echo "checkbox"; break;
								case "id";
								case "date_added"   :echo "hidden"; break;
								default: echo "text";
							} ?> name=<?= $key ?> value=<?php  echo '"'. htmlspecialchars($item[$key]) .'"';
							if (($key=="is_done")&&($item[$key]==1)){echo " checked" ;};?>><?php if (!array_key_exists($key,$editField)){echo $item[$key];} ?>
						</li>							
						<?php } ?>	
						<li ><input type="submit" name="submitChange" value="Сохранить">	</li>
						<li ><input type="submit" name="submitDel" value="Удалить"></li>
					</ul>	
				</form>
				<?php }?>		
			</div>
			<form action="formEdit.php" method="post">
				<input type="submit" name="submitAdd" value="Создать новое задание">
			</form>
		</body>
