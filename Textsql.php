<?php
namespace source; 

class Todo{
	private $pdo = null;
	function __construct($dbName,$login,$password){
		$this->pdo = new \PDO("mysql:host=localhost;dbname=" . $dbName, $login, $password);
		$this->pdo->query("SET NAMES UTF8");
	}
	public function getFullTable($orderField=""){
		$tsql = "SELECT `id`,`description`,`is_done` as `is_done`,`date_added`   FROM tasks";

		if (!$orderField=="") {
			$tsql = $tsql . " ORDER BY ". $orderField;
		}
		return $this->pdo->prepare($tsql) ;
	}
	public function addRecord($description,$is_done){

		$res = $this->pdo->prepare("INSERT  INTO  tasks (`description`,`is_done`,`date_added`) VALUES (:description, :is_done, now())  ") ;
		$res->bindValue(':description', htmlspecialchars($description) , \PDO::PARAM_STR);
		$res->bindValue(':is_done', htmlspecialchars($is_done) , \PDO::PARAM_INT);
		$res->execute();
	}

	public function changeRecord($id,$description,$is_done){
   	$res = $this->pdo->prepare("UPDATE tasks SET 
   		`description` = :description,
			`is_done`     = :is_done
				where id = :id      ") ;
		$res->bindValue(':description', htmlspecialchars($description) , \PDO::PARAM_STR);
		$res->bindValue(':is_done', htmlspecialchars($is_done) , \PDO::PARAM_INT);
		$res->bindValue(':id', $id , \PDO::PARAM_INT);
		$res->execute();
	}
	public function delRecord($id){

		$res = $this->pdo->prepare("DELETE  FROM tasks 
																where id = :id      ") ;
		$res->bindValue(':id', $id , \PDO::PARAM_INT);
		$res->execute();
	}
}


function getHeader() {
	return array("id"=>"номер","description" =>"описание","is_done"=>"Выполнено","date_added"=>"добавлено");
}
function getEditField() {
	return array("description" =>"Содержание задачи","is_done"=>"Выполнено");
}
function getShowField() {
	return array("id"=>"номер","date_added"=>"дата добавления");
}



